#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>

#include "parser.h"

typedef struct Tags Tag;

typedef struct Pages {
    char link[ 100 ];
    char title[ 100 ];
    char description[ 500 ];
    Tag * tags[ 20 ];
    int tagCount;
} Page;

typedef struct Tags {
    char name[ 100 ];
    char link[ 100 ];
    Page * pages[ 100 ];
    int pageCount;
} Tag;

struct Map {
    Tag tags[ 200 ];
    Page pages[ 200 ];
    int tagCount;
    int pageCount;
} map;

struct PageLayout {
	char  top[ 10000 ];
	char  bottom[ 1000 ];
} layout;

char * outDir;
char * template;

Tag * findTag( char * t ) {
    int index = 0;
    for ( index = 0; index < map.tagCount; index ++ ){
        if ( strcmp( map.tags[ index ].name, t ) == 0 ) {
            return &map.tags[ index ];
        }
    }
    return NULL;
}

int hasExt( const char * fp, const char * ext ) {

	char * fext = strrchr( fp, '.' );

	if ( fext == NULL ) {
		return 0;
	}

	if ( strncmp( fext, ext, strlen( ext ) ) == 0 ) {
		return 1;
	}

	return 0;
	
}

int getLayout() {

	char lfile[20000];

    FILE * fp = fopen( template, "r" );
    if(fp == NULL) {
        perror( "Unable to open template." );
		exit( EXIT_FAILURE );
    }
    fread(lfile, sizeof(char), 20000, fp);
    fclose(fp);

	int index = 0;

	while ( lfile[ index ] != '\0' ) {

		if ( lfile[ index ] == '{' && lfile[ index + 1 ] == '{' ) {

			lfile[ index ] = '\0';
			sprintf( layout.top, "%s", lfile );

			index++;

			while ( lfile[ index ] != '\0' ) {

				if ( lfile[ index ] == '}' && lfile[ index + 1 ] == '}' ) {

					sprintf( layout.bottom, "%s", &lfile[ index + 2 ] );		

					return 1;

				}

				index++;
				
			}
			
		}
		
		index++;
			
	}

	return 0;
	
}

void createTagLink( char * op,  char * tn ) {
	int index = 0;
	char tmp[100];
	sprintf( tmp, "%s", tn );
	while ( tmp[index++] != '\0' ) {
		if ( tmp[index] == ' '){
			tmp[index] = '_';
		} else if ( tmp[index] == '\n') {
			tmp[index] = '\0';
			break;
		}
	}
	sprintf( op, "tag_%s.html", tmp );
}

void createLinkLine( char * o, char * t, char * l ) {
	sprintf(o, "<a href=\"%s\">{%s}</a><br>", l, t );
}

Page * scanHeader( FILE * fp ) {
    
    size_t len = 1000;

    char * line = ( char * ) malloc( len );
    if ( line == NULL ) {
        printf( "Unable to allocate memory for header parse.\n" );
        exit( EXIT_FAILURE );
    }

    getline( &line, &len, fp );
        
    if ( strcmp( line, "~~~\n" ) != 0 ) {
        rewind( fp );
        return NULL;
    }

    Tag * tg;
    Page * pg = &map.pages[map.pageCount++];

    while ( getline( &line, &len, fp ) > 0 ) {
        int index = 0;
		while (line[index++] != '\0') {
			if (line[index]=='\n'){
				line[index] = '\0';
				break;
			}
		}
		index = 0;
        if ( strcmp( line, "~~~" ) == 0 ) {
            return pg;
        }


        switch ( line[ 0 ] ) {
            
            case 'T':
                while ( line[ ++index ] == ' ' );
                strcpy( pg->title, &line[ index ] );        
                break;
            
            case 'O':
                while ( line[ ++index ] == ' ' );
                strcpy( pg->link, &line[ index ] );
                break;

            case 'D':
                while ( line[ ++index ] == ' ' );
                strcpy( pg->description, &line[ index ] );
                break;

            case '#':
                while ( line[ ++index ] == ' ' );
                tg = findTag( &line[ index ] );
                if ( tg == NULL ) {
                    tg = &map.tags[ map.tagCount++ ];
                    sprintf(tg->name, "%s" ,&line[ index ]);
                    createTagLink( tg->link, tg->name );
                }
                tg->pages[ tg->pageCount++ ] = pg;
                pg->tags[pg->tagCount++] = tg;
                break;

            default:
                break;
        }

    }

    free( line );
    return pg;
}

void createFile( Page * pg, char * content ) {
	char outFile[200];
	sprintf(outFile, "%s%s", outDir, pg->link);
	FILE * fp = fopen( outFile, "w" );
	if ( fp == NULL ) {
		perror( "Unable to create output file" );
		exit( EXIT_FAILURE );
	}
	fprintf(fp, "%s\n%s\n", layout.top, content);
	fprintf(fp, "<hr>\n");
	fprintf(fp, "<h2>Topics:</h2>\n");
	int index = 0;
	for ( index = 0; index < pg->tagCount; index++ ) {
		char ln[200];
		createLinkLine( ln, pg->tags[index]->name, pg->tags[index]->link );
		fprintf(fp, ln);
	}
	fprintf(fp, "%s", layout.bottom);
	fclose(fp);
}

void scanDirectory( void ) {
    DIR *dfd;
    struct dirent * dp;

    if ( ( dfd = opendir( "." ) ) == NULL ) {
        printf( "unable to open directory" );
        exit( EXIT_FAILURE );
    }

    while ( ( dp = readdir( dfd ) ) != NULL ) {
        struct stat stbuf ;
        // sprintf( filename_qfd , "%s/%s",dir,dp->d_name) ;
        if( stat(dp->d_name, &stbuf ) == -1 ) {
            printf("Unable to stat file: %s\n", dp->d_name) ;
            continue ;
        }

        if ( ( stbuf.st_mode & S_IFMT ) == S_IFDIR ) {
            continue;
        } else {
            if ( hasExt( dp->d_name, ".liz" ) ) {
                FILE * fp = fopen( dp->d_name, "r");
                if ( fp != NULL ) {
                    Page * pg = scanHeader( fp );
                    
                    char * fc = ( char * ) calloc( 50000, sizeof(char) );
                    if ( fc == NULL ) {
                        printf( "Unable to allocate memory for file contents.\n" );
                        exit( EXIT_FAILURE );
                    }

                    fread(fc, sizeof(char), 50000, fp);
                    fclose(fp);

                    char * fo = parse( fc );
					
                    free(fc);
                    fc = NULL;

					createFile( pg, fo );

                }
            }
        }
    }
}

void createTagPages( void ) {
	int index = 0;
	Tag * tg;
	for (index = 0; index < map.tagCount; index++ ) {
		tg = &map.tags[index];
		char fn[200];
		sprintf(fn, "%s%s", outDir, tg->link);
		FILE * fp = fopen( fn, "w");
	 	if ( fp != NULL ) {
			fprintf(fp, "%s\n", layout.top);
			fprintf(fp, "<h1>%s</h1>\n", tg->name);
			fprintf(fp, "<p>\n");
			int i;
			for (i = 0; i < tg->pageCount; i++) {
				fprintf(fp, "<a href=\"%s\">{%s}</a> - %s <br>", tg->pages[i]->link, tg->pages[i]->title, tg->pages[i]->description);
			}
			fprintf(fp, "</p>\n");
			fprintf(fp, "%s\n", layout.bottom);
	 	}
	}
}

void createIndex( void ) {
	int index = 0;
	Tag * tg;
	char fn[200];
	sprintf(fn, "%sindex.html", outDir );
	FILE * fp = fopen( fn, "w");
 	if ( fp != NULL ) {
		fprintf(fp, "%s\n", layout.top);
		for (index = 0; index < map.tagCount; index++ ) {
			tg = &map.tags[index];
			fprintf(fp, "<h1>%s</h1>\n", tg->name);
			fprintf(fp, "<p>\n");
			int i;
			for (i = 0; i < tg->pageCount; i++) {
				fprintf(fp, "<a href=\"%s\">{%s}</a> - %s <br>", tg->pages[i]->link, tg->pages[i]->title, tg->pages[i]->description);
			}
			fprintf(fp, "</p>\n");
	 	}
		fprintf(fp, "%s\n", layout.bottom);
	}
}

void listTags( void ) {
	int index = 0;
	Tag * tg;
	char fn[200];
	sprintf(fn, "%stags.html", outDir );
	FILE * fp = fopen( fn, "w");
 	if ( fp != NULL ) {
		fprintf(fp, "%s\n", layout.top);
        fprintf(fp, "<h1>Topics.</h1>\n" );
		for (index = 0; index < map.tagCount; index++ ) {
			tg = &map.tags[index];
            fprintf(fp, "<a href=\"%s\">{%s}</a><br>", tg->link, tg->name);
	 	}
		fprintf(fp, "%s\n", layout.bottom);
	}
}



int main( int argc, char ** argv ) {

    int opt;
    int sl;

    while ((opt = getopt(argc, argv, "o:t:")) != -1) {
        
        switch (opt) {
            case 'o':
                sl = strlen( optarg );
                outDir = ( char * )malloc( 1000 );
                if ( outDir == NULL ) {
                    printf( "Error malloc out.\n" );
                    exit( EXIT_FAILURE );
                }
                if ( optarg[ sl ] != '/' ) {
                    sprintf( outDir, "%s/", optarg );
                } else {
                    strcpy( outDir, optarg );
                }
                break;

            case 't':
                sl = strlen( optarg );
                template = ( char * )malloc( sl + 1 );
                if ( template == NULL ) {
                    printf( "Error malloc input.\n" );
                    exit( EXIT_FAILURE );
                }
                sl = 0;
                strcpy( template, optarg );
                break;
            
            
            default: /* '?' */
                printf( "Unknown argument.\n" );
                exit( EXIT_FAILURE );
        }
    }

    getLayout();
    scanDirectory();
	createTagPages();
	createIndex();
	listTags();

    free( outDir );
    free(template);
}
