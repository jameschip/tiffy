#include <stdio.h>
#include "parser.h"

char in_p_block = 0;

char output[10000];
int out_index;

void add_output_str( const char *ch ) {

	int ind = 0;
	
	while ( ch[ ind ] != '\0' ) {
		output[ out_index++ ] = ch[ ind++ ];
	}

	output[ out_index ] = '\0';
	
}

void add_output_char( const char ch ) {

	output[ out_index++ ] = ch;
	output[ out_index ] = '\0';

}

void gobble( int *index, const char *str ) {

	for (; str[ *index ] != '\0'; *index += 1 ) {
		if ( str[ *index ] == '}' && str[ *index + 1] == '}' ) {
			*index += 1;
			break;
		}
		add_output_char( str[ *index ] );
	}
	
}

void go_end( int *index, const char *str ) {

	for (; str[ *index ] != '\0'; *index += 1 ) {
		if ( str[ *index ] == '}' && str[ *index + 1] == '}' ) {
			*index += 1;
			break;
		}
	}
	
}

void do_head_tag( int *index, const char *str ) {

	char tag[10];

	*index += 1;
	char hv = str[ *index ];
	*index += 1;
	while ( str[ *index ] == ' ' ) *index += 1;
	
	sprintf( tag, "<h%c>", hv );			
	add_output_str( tag );

	gobble( index, str );

	sprintf( tag, "</h%c>", hv );
	add_output_str( tag );							

}

void do_link_tag( int *index, const char *str ) {


	*index += 1;
	while ( str[ *index ] == ' ' ) *index += 1;

	add_output_str( "<a href=\"" );	

	for (; str[ *index ] != '\0'; *index += 1 ) {

		if ( str[ *index ] == ' ' ) {
			*index += 1;
			add_output_str( "\">{" );
			break;
			
		} else if ( str[ *index ] == '}' && str[ *index + 1] == '}' ) {
			*index += 1;
			break;
		}
		
		add_output_char( str[ *index ] );
	}

	gobble( index, str );
	
	add_output_str( "}</a>" );	
}

void do_style_tag( const char *style, int * index, const char * str ) {

	char tag[20];
	
	*index += 1;
	while ( str[ *index ] == ' ' ) *index += 1;

	sprintf( tag, "<%s>", style );
	add_output_str( tag );	

	gobble( index, str );

	sprintf( tag, "</%s>", style );
	add_output_str( tag );	
	
}

void do_image_tag( int* index, const char * str ) {

	*index += 1;
	while ( str[ *index ] == ' ' ) *index += 1;
	add_output_str( "<img src=\"" );	
	gobble( index, str );
	add_output_str( "\">" );
		
}

void do_bullet_tag( int *index, const char * str ) {

	int in_ul = 1;
	
	*index += 1;

	add_output_str( "<ul>" );	

   	while (in_ul) {

		while ( str[ *index ] == ' ' ) *index += 1;

		add_output_str( "<li>" );	

		gobble( index, str );
		
		add_output_str( "</li>" );		

		int tmpind = *index;

		while ( str[ *index ] != '\n' && str[ *index ] != '\0' ) *index += 1;
		*index += 1;
		
		if ( str[ *index ] == '{' && str[ *index + 1 ] == '{' ) {
			while ( str[ *index ] == '{' ) *index += 1;
			while ( str[ *index ] == ' ' ) *index += 1;
			
			if ( str[ *index ] == 'b' && str[ *index + 1 ] == 'p' ) {
				*index += 2;
				continue;
			} else {
				in_ul = 0;
				*index = tmpind;	
			}
			
		} else {
			in_ul = 0;
			*index = tmpind;
		}
   			
   	}

	add_output_str( "</ul>" );
		
}

void do_break_tag( int * index, const char * str ) {

	add_output_str( "<br>" );
	*index += 1;
	while ( str[ *index ] == ' ' ) *index += 1;
	
	go_end( index, str );
	
}

void do_tag( int *index, const char *str ) {

	while ( str[ *index ] == '{' ) *index += 1;
	while ( str[ *index ] == ' ' ) *index += 1;

	switch ( str[ *index ] ) {

		case 'h':	
		case 'H':

			if ( in_p_block ) {
				add_output_str( "</p>" );
				in_p_block = 0;
			}
			
			do_head_tag( index, str );
			
			break;

		case 'l':
		case 'L':

			if ( !in_p_block ) {
				add_output_str( "<p>" );
				in_p_block = 1;
			}
			
			if ( str[ *index + 1 ] == 'i' ) {
				*index += 1;
				do_link_tag( index, str );
			}
			
			break;

		case 'b':
		case 'B':
		
			if ( str[ *index + 1 ] == 'o' ) {
				if ( !in_p_block ) {
					add_output_str( "<p>" );
					in_p_block = 1;
				}
				*index += 1;
				do_style_tag( "b", index, str );

			} else if ( str[ *index +1 ] == 'q' ) {
				*index += 1;
				do_style_tag( "blockquote", index, str );
				
			} else if ( str[ *index + 1 ] == 'p' ) {
				*index += 1;
				do_bullet_tag( index, str );
				
			} else if ( str[ *index + 1 ]== 'r' ) {
				do_break_tag( index, str );
			}
			
			break;

		case 'c':
		case 'C':

			if ( str[ *index + 1 ] == 'b' ) {
				if ( in_p_block ) {
					add_output_str( "</p>" );
					in_p_block = 0;
				}
				*index += 1;
				do_style_tag( "pre", index, str );
				
			}
			
			break;
		
		case 'i':
		case 'I':
		
			if ( str[ *index + 1 ] == 't' ) {
				if ( !in_p_block ) {
					add_output_str( "<p>" );
					in_p_block = 1;
				}
				*index += 1;
				do_style_tag( "em", index, str );
				
			} else if ( str[ *index + 1 ] == 'm' ) {
				*index += 1;
				do_image_tag( index, str );
			}
			
			break;
	}

} 

char * parse ( const char *input ) {

	output[0] = '\0';
	out_index = 0;
	
	int in_index = 0;
		
	while ( input[ in_index ] != '\0' ) {

			if ( input[ in_index ] == '{' && input[ in_index + 1 ] == '{' ) {
				if ( input[ in_index - 1] == '/' ) {
					out_index--;
					add_output_char( input[ in_index ] );
				} else {
					do_tag( &in_index, input );
				}
				
			} else if  ( input[ in_index ] == '\n' ) {

				if ( input[ in_index + 1 ] == '\n' && in_p_block ) {

					add_output_str( "</p>" );
					in_p_block = 0;
					
				} else if ( in_p_block ) {

					add_output_str( "<br>" );
					
				}
				
				
			} else {

				if ( !in_p_block ) {
					add_output_str( "<p>" );
					in_p_block = 1;
				}

				add_output_char( input[ in_index ] );
				
			}

			in_index++;

	}

	if ( in_p_block ) {
		add_output_str( "</p>" );
		in_p_block = 0;
	}

	return output;		
}
