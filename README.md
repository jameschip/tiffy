# TIFFY

A static website generator. Lives on the river, fed by the lizzard.

Creates static pages from all of your liz files in a directory and links them all together with tags.

Uses the headers in the liz files to create the output files and links.

## Compile

gcc main.c parser.c -o tiffy

## Use

tiffy -t templateFile.html -o outDir

